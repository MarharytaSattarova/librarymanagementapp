﻿using Library.BLL.Services;
using Library.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class ServiceTests
    {
        readonly CommentsService commentsService;
        readonly NewsService newsService;

        public ServiceTests()
        {
            commentsService = new CommentsService();
            newsService = new NewsService();
        }

        [Fact]
        public void GetCommentsOrderedByPublicationDateDescending_WhenCalled_ReturnsCommentsInTheCorrectOrder()
        {
            var expected = commentsService.UnitOfWork.Comments.Get().OrderByDescending(x => x.PublicationDate).ToList();

            var actual = commentsService.GetCommentsOrderedByPublicationDateDescending().ToList();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddComment_WhenCalled_ThenCommentsCountChanged()
        {
            var initialCommentsCount = commentsService.UnitOfWork.Comments.Get().Count;

            commentsService.AddComment("User Name", "Comment body");
            var commentsCount = commentsService.UnitOfWork.Comments.Get().Count;

            var addedComment = commentsService.UnitOfWork.Comments.Get().FirstOrDefault(x => x.AuthorName == "User Name" && x.Body == "Comment body");
            commentsService.UnitOfWork.Comments.Delete(addedComment);

            Assert.NotEqual(initialCommentsCount, commentsCount);
        }

        [Fact]
        public void AddComment_WhenCalled_ThenCommentsListContainsAddedComment()
        {
            commentsService.AddComment("User Name", "Comment body");

            var addedComment = commentsService.UnitOfWork.Comments.Get().FirstOrDefault(x => x.AuthorName == "User Name" && x.Body == "Comment body");
            commentsService.UnitOfWork.Comments.Delete(addedComment);

            Assert.NotNull(addedComment);
        }

        [Fact]
        public void GetCommentsOrderedByPublicationDateDescending_WhenCalled_ReturnsNewsInTheCorrectOrder()
        {
            var expected = newsService.UnitOfWork.NewsStories.Get().OrderByDescending(x => x.PublicationDate).ToList();

            var actual = newsService.GetNewsOrderedByPublicationDateDescending().ToList();

            Assert.Equal(expected, actual);
        }
    }
}
