using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Library.Tests
{
    public class LibraryTests
    {
        readonly BLL.Services.CommentsService commentsService;

        public LibraryTests()
        {
            commentsService = BLL.Services.CommentsService.Instance;
        }

        [Fact]
        public void Test1()
        {
            var expected = commentsService.UnitOfWork.Comments.Get().OrderByDescending(x => x.PublicationDate).ToList();

            var actual = commentsService.GetCommentsOrderedByPublicationDateDescending().ToList();

            Assert.Equal(expected, actual);
        }
    }
}
