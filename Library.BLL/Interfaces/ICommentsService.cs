﻿using Library.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces
{
    public interface ICommentsService
    {
        IEnumerable<DAL.Entities.Comment> GetCommentsOrderedByPublicationDateDescending();
        void AddComment(string authorName, string commentBody);
    }
}
