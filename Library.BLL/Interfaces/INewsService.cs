﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces
{
    public interface INewsService
    {
        IEnumerable<DAL.Entities.NewsStory> GetNewsOrderedByPublicationDateDescending();
    }
}
