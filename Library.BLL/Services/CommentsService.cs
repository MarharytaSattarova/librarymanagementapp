﻿using Library.BLL.Interfaces;
using Library.DAL.Interfaces;
using Library.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    /// <summary>
    /// Implements all the Guest View functionality that is available in the library application.
    /// </summary>
    public class CommentsService : ICommentsService
    {
        ///// <summary>
        ///// Stores the instance of the comment service.
        ///// </summary>
        //private static CommentsService instance;

        ///// <summary>
        ///// Stores object locker instance.
        ///// </summary>
        //private static readonly object locker = new object();

        /// <summary>
        /// Stores the instance of Unit Of Work.
        /// </summary>
        public IUnitOfWork UnitOfWork { get; set; } = new DAL.UnitOfWork.UnitOfWork();

        ///// <summary>
        ///// Hidden constructor of the CommentService class.
        ///// </summary>
        //CommentsService() { }

        /// <summary>
        /// Provides access to the comment service instance only for reading. If such instance does not exist yet, creates one.
        /// </summary>
        //public static CommentsService Instance
        //{
        //    get
        //    {
        //        lock (locker)
        //        {
        //            return instance ?? (instance = new CommentsService());
        //        }
        //    }
        //}

        /// <summary>
        /// Returns the collection of the comments order by descending by their publication date.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DAL.Entities.Comment> GetCommentsOrderedByPublicationDateDescending()
        {
            return UnitOfWork.Comments.Get().OrderByDescending(x => x.PublicationDate);
        }

        /// <summary>
        /// Method that adds the comment to the application database.
        /// </summary>
        /// <param name="authorName">Name of the person who wrote the comment</param>
        /// <param name="commentBody">Comment body content</param>
        public void AddComment(string authorName, string commentBody)
        {
            var newComment = new DAL.Entities.Comment()
            {
                AuthorName = authorName,
                PublicationDate = DateTime.Now,
                Body = commentBody
            };

            UnitOfWork.Comments.Create(newComment);
        }
    }
}
