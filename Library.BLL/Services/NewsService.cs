﻿using Library.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    /// <summary>
    /// Implements all the Home View functionality that is available in the library application.
    /// </summary>
    public class NewsService : INewsService
    {
        ///// <summary>
        ///// Stores the instance of the news service.
        ///// </summary>
        //private static NewsService instance;

        ///// <summary>
        ///// Stores object locker instance.
        ///// </summary>
        //private static readonly object locker = new object();

        /// <summary>
        /// Stores the instance of Unit Of Work.
        /// </summary>
        public DAL.Interfaces.IUnitOfWork UnitOfWork { get; set; } = new DAL.UnitOfWork.UnitOfWork();

        ///// <summary>
        ///// Hidden constructor of the NewsService class.
        ///// </summary>
        //NewsService() { }

        ///// <summary>
        ///// Provides access to the news service instance only for reading. If such instance does not exist yet, creates one.
        ///// </summary>
        //public static NewsService Instance
        //{
        //    get
        //    {
        //        lock (locker)
        //        {
        //            return instance ?? (instance = new NewsService());
        //        }
        //    }
        //}

        /// <summary>
        /// Returns the collection of the news stories order by descending by their publication date.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DAL.Entities.NewsStory> GetNewsOrderedByPublicationDateDescending()
        {
            return UnitOfWork.NewsStories.Get().OrderByDescending(x => x.PublicationDate);
        }
    }
}
