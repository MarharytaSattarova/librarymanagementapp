﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using Library.Models;

namespace Library.Controllers
{
    /// <summary>
    /// Controller which operates the actions available for survey Form View.
    /// </summary>
    public class SurveyController : Controller
    {
        /// <summary>
        /// Stores the list of the user age options for the form.
        /// </summary>
        private readonly IEnumerable<string> ageList = new List<string> { "under 18", "18-30", "30-50", "over 50" };

        /// <summary>
        /// Stores the list of the user favorite genres options for the form.
        /// </summary>
        private readonly IEnumerable<string> genres = new List<string> { "Satire", "Fantasy", "Horror", "Crime/Detective" };

        /// <summary>
        /// Method which displays survey form view.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            ViewBag.AgeList = ageList;
            ViewBag.Genres = genres;

            return View();
        }

        /// <summary>
        /// Method which gets called whenever survey form is submitted by the user.
        /// </summary>
        /// <param name="formCollection">collection containing the values of the submitted form fields.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Form(FormCollection formCollection)
        {
            var surveyResult = new Models.SurveyResult()
            {
                AuthorName = formCollection["name"],
                AuthorSurname = formCollection["surname"],
                AuthorAge = formCollection["age"],
                FavoriteGenres = genres.Where(genre => formCollection[genre] != null).ToList()
            };

            if (TryValidateModel(surveyResult))
            {
                return View("SurveyResult", surveyResult);
            }

            return RedirectToAction("Form", "Survey");
        }
    }
}