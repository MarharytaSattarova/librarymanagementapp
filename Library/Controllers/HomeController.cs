﻿using Library.BLL.Interfaces;
using Library.BLL.Services;
using Library.DAL.Entities;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    /// <summary>
    /// Controller which operates the actions available for Home View.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Stores news service instance.
        /// </summary>
        private INewsService newsService;

        /// <summary>
        /// Constructor which creates an instance of HomeController class.
        /// </summary>
        public HomeController(INewsService service)
        {
            newsService = service;
        }

        /// <summary>
        /// Method which is used to display Home(Index) page.
        /// </summary>
        /// <returns>About view</returns>
        public ActionResult Index()
        {
            var news = newsService.GetNewsOrderedByPublicationDateDescending();

            var newsMapped = new List<Models.NewsStory>();

            foreach (var item in news)
            {
                var mappedItem = new Models.NewsStory()
                {
                    AuthorName = item.AuthorName,
                    AuthorSurname = item.AuthorSurname,
                    PublicationDate = item.PublicationDate,
                    Title = item.Title,
                    Body = item.Body
                };

                newsMapped.Add(mappedItem);
            }
            return View(newsMapped);
        }

        public ActionResult ViewCompleteNewsStory(NewsStory story)
        {
            var mappedItem = new Models.NewsStory()
            {
                AuthorName = story.AuthorName,
                AuthorSurname = story.AuthorSurname,
                PublicationDate = story.PublicationDate,
                Title = story.Title,
                Body = story.Body
            };

            return View("CompleteNewsStoryView", mappedItem);
        }
    }
}