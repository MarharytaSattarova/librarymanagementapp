﻿using Library.BLL.Interfaces;
using Library.BLL.Services;
using Library.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Library.Controllers
{
    /// <summary>
    /// Controller which operates the actions available for Guest View.
    /// </summary>
    public class GuestController : Controller
    {
        /// <summary>
        /// Stores comment service instance.
        /// </summary>
        private ICommentsService commentsService;

        /// <summary>
        /// Constructor which creates an instance of GuestController class.
        /// </summary>
        public GuestController(ICommentsService service)
        {
            commentsService = service;
        }

        /// <summary>
        /// Method which is used to display About page.
        /// </summary>
        /// <returns>About view</returns>
        [HttpGet]
        public ActionResult About()
        {
            var comments = commentsService.GetCommentsOrderedByPublicationDateDescending();

            var commentsMapped = new List<Models.Comment>();

            foreach (var item in comments)
            {
                var mappedItem = new Models.Comment()
                {
                    AuthorName = item.AuthorName,
                    PublicationDate = item.PublicationDate,
                    Body = item.Body
                };

                commentsMapped.Add(mappedItem);
            }

            return View(commentsMapped);
        }

        /// <summary>
        /// Method which is used to add a comment to the list of comments on the About page.
        /// </summary>
        /// <param name="newComment">Comment to add</param>
        /// <returns>Adds new comment to the About get method if comment model is valid, and then redirects to the About get method.</returns>
        [HttpPost]
        public ActionResult About(Models.Comment newComment)
        {
            if (!ModelState.IsValidField("AuthorName"))
            {
                ModelState.AddModelError("", "Must be a valid name.");
            }

            if (!ModelState.IsValidField("Body"))
            {
                ModelState.AddModelError("", "Must be a valid comment body.");
            }

            if (ModelState.IsValid)
            {
                commentsService.AddComment(newComment.AuthorName, newComment.Body);
            }

            return RedirectToAction("About", "Guest");
        }

        /// <summary>
        /// Method used to perform the remote validation of UserName field.
        /// </summary>
        /// <param name="AuthorName">Author name to validate</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ValidateAuthorName(string AuthorName)
        {
            var pattern = @"^([А-Я]{1}[а-яё]{1,27}|[A-Z]{1}[a-z]{1,27}|[А-Я]{1}[а-яё]{1,27} [А-Я]{1}[а-яё]{1,27}|[A-Z]{1}[a-z]{1,27} [A-Z]{1}[a-z]{1,27})$";

            AuthorName = Request.QueryString["newComment.AuthorName"];

            if (!Regex.IsMatch(AuthorName, pattern))
            {
                return Json("Must be a valid name.", JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Method used to perform the remote validation of comment Body field.
        /// </summary>
        /// <param name="Body">Comment body to validate</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ValidateCommentBody(string Body)
        {
            var pattern = @"^[a-zA-Zа-яёА-Я][^<>]+$";

            Body = Request.QueryString["newComment.Body"];

            if (!Regex.IsMatch(Body, pattern))
            {
                return Json("Must be a valid comment body.", JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}