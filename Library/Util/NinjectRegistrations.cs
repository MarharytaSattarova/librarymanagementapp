﻿using Library.BLL.Interfaces;
using Library.BLL.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Util
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<ICommentsService>().To<CommentsService>();
            Bind<INewsService>().To<NewsService>();
        }
    }
}