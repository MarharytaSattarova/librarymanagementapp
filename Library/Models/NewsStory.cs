﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    /// <summary>
    /// Model containing the information about the news story.
    /// </summary>
    public class NewsStory
    {
        /// <summary>
        /// Stores the news story title.
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Stores the name of the news story author.
        /// </summary>
        [Required]
        public string AuthorName { get; set; }

        /// <summary>
        /// Stores the surname of the news story author.
        /// </summary>
        [Required]
        public string AuthorSurname { get; set; }

        /// <summary>
        /// Stores the news story publication date.
        /// </summary>
        [DataType(DataType.Date)]
        public DateTime PublicationDate { get; set; }

        /// <summary>
        /// Stores the news story content.
        /// </summary>
        [Required]
        public string Body { get; set; }
    }
}