﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    /// <summary>
    /// Model containing the information about the survey result.
    /// </summary>
    public class SurveyResult
    {
        /// <summary>
        /// Stores the name of the person who took the survey.
        /// </summary>
        [Required(ErrorMessage = "Name is required.")]
        [RegularExpression("^([А-Я]{1}[а-яё]{1,27}|[A-Z]{1}[a-z]{1,27})$", ErrorMessage = "Has to be a valid name.")]
        public string AuthorName { get; set; }

        /// <summary>
        /// Stores the surname of the person who took the survey.
        /// </summary>
        [Required(ErrorMessage = "Surname is required.")]
        [RegularExpression("^([А-Я]{1}[а-яё]{1,27}|[A-Z]{1}[a-z]{1,27})$", ErrorMessage = "Has to be a valid surname.")]
        public string AuthorSurname { get; set; }

        /// <summary>
        /// Stores the age of the person who took the survey.
        /// </summary>
        public string AuthorAge { get; set; }

        /// <summary>
        /// Stores favorite book genres of the person who took the survey.
        /// </summary>
        public virtual ICollection<string> FavoriteGenres { get; set; }
    }
}