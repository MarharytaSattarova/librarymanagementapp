﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Library.Models
{
    /// <summary>
    /// Model containing the information about the comment.
    /// </summary>
    public class Comment
    {
        /// <summary>
        /// Stores the name of the user who wrote the comment.
        /// </summary>
        [Required]
        [RegularExpression("^([А-Я]{1}[а-яё]{1,27}|[A-Z]{1}[a-z]{1,27}|[А-Я]{1}[а-яё]{1,27} [А-Я]{1}[а-яё]{1,27}|[A-Z]{1}[a-z]{1,27} [A-Z]{1}[a-z]{1,27})$")]
        [Remote("ValidateAuthorName", "Guest")]
        public string AuthorName { get; set; }

        /// <summary>
        /// Stores the comment publication date.
        /// </summary>
        public DateTime PublicationDate { get; set; }

        /// <summary>
        /// Stores the comment body content.
        /// </summary>
        [Required]
        [RegularExpression("^[a-zA-Zа-яёА-Я][^<>]+$")]
        [Remote("ValidateCommentBody", "Guest")]
        public string Body { get; set; }
    }
}