﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Helpers
{
    /// <summary>
    /// Class containing an extention method for creating HTML list items.
    /// </summary>
    public static class ListHelper
    {
        /// <summary>
        /// Helper extension method to create HTML list items.
        /// </summary>
        /// <param name="html">Htmlhelper instance</param>
        /// <param name="values">Values to use when creating list items</param>
        /// <returns>Html string</returns>
        public static MvcHtmlString CreateUl(this HtmlHelper html, List<string> values)
        {
            var ul = new TagBuilder("ul");

            foreach (var item in values)
            {
                var li = new TagBuilder("li");
                li.SetInnerText(item);
                ul.InnerHtml += li;
            }

            return new MvcHtmlString(ul.ToString());
        }
    }
}