﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Entities
{
    /// <summary>
    /// Business entity containing all the information about the comment.
    /// </summary>
    public class Comment : BaseEntity
    {
        /// <summary>
        /// Stores the name of the user who wrote the comment.
        /// </summary>
        public string AuthorName { get; set; }
        /// <summary>
        /// Stores comment's publication date.
        /// </summary>
        public DateTime PublicationDate { get; set; }
        /// <summary>
        /// Stores comment's body content.
        /// </summary>
        public string Body { get; set; }
    }
}
