﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Entities
{
    /// <summary>
    /// Business entity containing all the information about the survey.
    /// </summary>
    public class SurveyResult : BaseEntity
    {
        /// <summary>
        /// Stores the name of the person who took the survey.
        /// </summary>
        public string AuthorName { get; set; }
        /// <summary>
        /// Stores the surname of the person who took the survey.
        /// </summary>
        public string AuthorSurname { get; set; }
        /// <summary>
        /// Stores the age of the person who took the survey.
        /// </summary>
        public string AuthorAge { get; set; }
        /// <summary>
        /// Stores favorite book genres of the person who took the survey.
        /// </summary>
        public virtual ICollection<string> FavoriteGenres { get; set; }
    }
}
