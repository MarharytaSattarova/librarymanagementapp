﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Entities
{
    /// <summary>
    /// Serves as the base class to all the business entities present in the program.
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Stores the unique Id for each entity instance.
        /// </summary>
        public int ID { get; set; }
    }
}
