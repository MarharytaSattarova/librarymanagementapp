﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Entities
{
    /// <summary>
    /// Business entity containing all the information about the news story.
    /// </summary>
    public class NewsStory : BaseEntity
    {
        /// <summary>
        /// Stores the news story title.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Stores the name of the news story author.
        /// </summary>
        public string AuthorName { get; set; }
        /// <summary>
        /// Stores the surname of the news story author.
        /// </summary>
        public string AuthorSurname { get; set; }
        /// <summary>
        /// Stores the news story publication date.
        /// </summary>
        public DateTime PublicationDate { get; set; }
        /// <summary>
        /// Stores the news story content.
        /// </summary>
        public string Body { get; set; }
    }
}
