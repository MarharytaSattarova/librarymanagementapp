﻿using Library.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.DataSource
{
    /// <summary>
    /// Stores seeding data for the library.
    /// </summary>
    public static class DataSource
    {
        /// <summary>
        /// Seeding data containing news stories information and content.
        /// </summary>
        public static List<NewsStory> NewsStories { get; set; } = new List<NewsStory>()
        {
            new NewsStory()
            {
                Title = "News Story Title 1",
                AuthorName = "Hilton",
                AuthorSurname = "Kemmer",
                PublicationDate = new DateTime(2020, 10, 6),
                Body = "Duis posuere purus in neque tempus facilisis. Mauris ac urna in ligula pharetra interdum ac eu sem." +
                    "Nullam consequat porta efficitur. Nunc iaculis mauris at sem posuere auctor. Donec lacinia nibh libero, eu porttitor eros maximus eu." +
                    "Donec placerat tincidunt eros eu varius. Proin eu nulla sed nibh suscipit dignissim. Nam a turpis ac nibh volutpat faucibus." +
                    "Nullam varius euismod erat ut vulputate. Fusce id leo dui. Nam pharetra molestie sodales. Sed sed magna pretium justo fringilla vehicula eu sed odio."
            },

            new NewsStory()
            {
                Title = "News Story Title 2",
                AuthorName = "Kirsten",
                AuthorSurname = "Kemmer",
                PublicationDate = new DateTime(2020, 10, 5),
                Body = "Aenean vel mattis est, vel maximus ex. Pellentesque aliquam tincidunt arcu id imperdiet. Proin viverra neque at velit ornare, eget pretium lectus fermentum." +
                    "Fusce iaculis id libero vitae faucibus. Etiam imperdiet cursus ex, sit amet mattis nulla consectetur in. Maecenas aliquam ante at est commodo eleifend." +
                    "Nullam nisl arcu, rhoncus ut ullamcorper sit amet, vulputate id orci. Maecenas quam quam, aliquet eu velit vitae, eleifend sodales turpis."
            },

            new NewsStory()
            {
                Title = "News Story Title 3",
                AuthorName = "Issac",
                AuthorSurname = "Rath",
                PublicationDate = new DateTime(2020, 10, 1),
                Body = "Nullam tempus quam sed urna placerat lacinia. Sed nisl felis, aliquam vel arcu quis, tristique rutrum sapien. In in mauris dolor." +
                    "Suspendisse ut felis diam. Vivamus quis mauris tempus, commodo risus et, dapibus ipsum. Etiam mattis luctus vehicula." +
                    "Praesent vel est vitae ex commodo pellentesque ac et libero. Duis imperdiet vestibulum sapien, id porttitor sapien lobortis ut." +
                    "Cras ac eros sed dui elementum mollis. Aliquam dignissim neque sem, non sodales ex vulputate a. Nam eget libero ullamcorper, dignissim" +
                    "enim eu, rutrum augue. Donec a dui ultricies, semper quam ac, dignissim massa. Aliquam erat volutpat. Suspendisse nulla velit, elementum ut rutrum et," +
                    "sodales sit amet lacus. Sed dignissim nisi bibendum turpis varius, vitae imperdiet risus pharetra. Sed consequat ac enim at laoreet."
            }
        };

        /// <summary>
        /// Seeding data containing comments information and content.
        /// </summary>
        public static List<Comment> Comments { get; set; } = new List<Comment>()
        {
            new Comment()
            {
                AuthorName = "Sherman",
                PublicationDate = new DateTime(2020, 10, 5),
                Body = "Suspendisse ut felis diam. Vivamus quis mauris tempus, commodo risus et, dapibus ipsum. Etiam mattis luctus vehicula." +
                       "Praesent vel est vitae ex commodo pellentesque ac et libero. Duis imperdiet vestibulum sapien, id porttitor sapien lobortis ut." +
                       "Cras ac eros sed dui elementum mollis. Aliquam dignissim neque sem, non sodales ex vulputate a."
            },

            new Comment()
            {
                AuthorName = "Jayde",
                PublicationDate = new DateTime(2020, 10, 3),
                Body = "Mauris quis viverra dui. Cras pellentesque a nulla non congue. Etiam ultrices neque nulla, ut elementum dui consequat quis."
            },

            new Comment()
            {
                AuthorName = "Edward",
                PublicationDate = new DateTime(2020, 10, 2),
                Body = "Vestibulum non molestie odio. Proin venenatis lacus sed tempor blandit. In consectetur nibh in sodales gravida." +
                       "Pellentesque non diam placerat, mollis nulla et, iaculis metus. Donec vel viverra sem. Sed dignissim arcu enim." +
                       "Pellentesque varius magna sed condimentum varius."
            },

            new Comment()
            {
                AuthorName = "Madison",
                PublicationDate = new DateTime(2020, 10, 2),
                Body = "Vivamus quis mauris tempus, commodo risus et, dapibus ipsum. Etiam mattis luctus vehicula."
            }
        };

        /// <summary>
        /// Seeding data containing survey results information and content.
        /// </summary>
        public static List<SurveyResult> SurveyResults { get; set; } = new List<SurveyResult>() { };
    }
}
