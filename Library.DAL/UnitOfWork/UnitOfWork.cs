﻿using Library.DAL.Context;
using Library.DAL.Entities;
using Library.DAL.Interfaces;
using Library.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.UnitOfWork
{
    /// <summary>
    /// Provides access to the data of all the repositories in the application.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Provides access to the data of all the repositories in the application.
        /// </summary>
        private readonly ApplicationContext _context;

        /// <summary>
        /// Stores the instance of comment repository.
        /// </summary>
        private CommentRepository _commentRepository;

        /// <summary>
        /// Stores the instance of news story repository.
        /// </summary>
        private NewsStoryRepository _newsStoryRepository;

        /// <summary>
        /// Stores the instance of survey result repository.
        /// </summary>
        private SurveyResultRepository _surveyResultRepository;

        /// <summary>
        /// Constructor that creates an instance of UnitOfWork.
        /// </summary>
        public UnitOfWork()
        {
            _context = new ApplicationContext();
        }

        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{Comment}"/> interface and provides access only for reading. If the instance of comment repository does not exist, than creates it.
        /// </summary>
        public IRepository<Comment> Comments
        {
            get
            {
                if (_commentRepository == null)
                    _commentRepository = new CommentRepository(_context);
                return _commentRepository;
            }
        }

        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{NewsStory}"/> interface and provides access only for reading. If the instance of news story repository does not exist, than creates it.
        /// </summary>
        public IRepository<NewsStory> NewsStories
        {
            get
            {
                if (_newsStoryRepository == null)
                    _newsStoryRepository = new NewsStoryRepository(_context);
                return _newsStoryRepository;
            }
        }

        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{SurveyResult}"/> interface and provides access only for reading. If the instance of survey result repository does not exist, than creates it.
        /// </summary>
        public IRepository<SurveyResult> SurveyResults
        {
            get
            {
                if (_surveyResultRepository == null)
                    _surveyResultRepository = new SurveyResultRepository(_context);
                return _surveyResultRepository;
            }
        }

        /// <summary>
        /// Method used to apply changes made in the database context.
        /// </summary>
        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
