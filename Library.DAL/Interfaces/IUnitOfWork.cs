﻿using Library.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces
{
    /// <summary>
    /// Contains the functionality that UnitOfWork must have.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{NewsStory}"/> interface and provides access only for reading.
        /// </summary>
        IRepository<NewsStory> NewsStories { get; }

        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{Comment}"/> interface and provides access only for reading.
        /// </summary>
        IRepository<Comment> Comments { get; }

        /// <summary>
        /// Stores an instance that implements <see cref="IRepository{SurveyResult}"/> interface and provides access only for reading.
        /// </summary>
        IRepository<SurveyResult> SurveyResults { get; }

        /// <summary>
        /// Method used to apply changes made in the database context.
        /// </summary>
        void SaveChanges();
    }
}
