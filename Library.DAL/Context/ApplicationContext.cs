﻿using Library.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Context
{
    /// <summary>
    /// Context that stores collections of comments, news stories and survey results.
    /// </summary>
    public class ApplicationContext : DbContext
    {
        /// <summary>
        /// Constructor that creates an instance of the application DB context.
        /// </summary>
        public ApplicationContext() : base("LibraryAdvancedContext")
        {

        }

        /// <summary>
        /// List of all news stories present in the system.
        /// </summary>
        public DbSet<NewsStory> NewsStories { get; set; }

        /// <summary>
        /// List of all comments present in the system.
        /// </summary>
        public DbSet<Comment> Comments { get; set; }

        /// <summary>
        /// List of all survey results present in the system.
        /// </summary>
        public DbSet<SurveyResult> SurveyResults { get; set; }

        /// <summary>
        /// Method that gets executed application DB context is created.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
