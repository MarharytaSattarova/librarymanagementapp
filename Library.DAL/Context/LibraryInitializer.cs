﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Context
{
    /// <summary>
    /// Class used to set up initialization options for the application database.
    /// </summary>
    public class LibraryInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ApplicationContext>
    {
        /// <summary>
        /// Method to initialize database with seeding data.
        /// </summary>
        /// <param name="context">Application DB context</param>
        protected override void Seed(ApplicationContext context)
        {
            var newsStories = Library.DAL.DataSource.DataSource.NewsStories;

            newsStories.ForEach(s => context.NewsStories.Add(s));
            context.SaveChanges();

            var comments = Library.DAL.DataSource.DataSource.Comments;

            comments.ForEach(c => context.Comments.Add(c));
            context.SaveChanges();
        }
    }
}
