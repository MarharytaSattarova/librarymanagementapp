﻿using Library.DAL.Context;
using Library.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    /// <summary>
    /// Repository to access and modify survey result instances in the application context.
    /// </summary>
    public class SurveyResultRepository : BaseRepository<SurveyResult>
    {
        /// <summary>
        /// Construstor that creates an instance of the survey result repository based on the ApplicationContext instance passed as argument.
        /// </summary>
        /// <param name="context">Application context instance with seed information.</param>
        public SurveyResultRepository(ApplicationContext context) : base(context) { }

        /// <summary>
        /// Searches for a survey result by Id and updates it's information with the information provided in the survey result object argument.
        /// </summary>
        /// <param name="entity">Survey result instance containig new survey result information.</param>
        /// <param name="id">Id of the survey result that has to be updated.</param>
        /// <exception cref="ArgumentException">Thrown when survey result with the provided Id is not found."</exception>
        public override void Update(SurveyResult entity, int id)
        {
            var searchedSurveyResult = context.Set<SurveyResult>().FirstOrDefault(x => x.ID == id);

            if (searchedSurveyResult == null) throw new ArgumentException("Survey Result with such ID not found.", "id");

            searchedSurveyResult.AuthorName = entity.AuthorName;
            searchedSurveyResult.AuthorSurname = entity.AuthorSurname;
            searchedSurveyResult.AuthorAge = entity.AuthorAge;
            searchedSurveyResult.FavoriteGenres = entity.FavoriteGenres;
        }
    }
}
