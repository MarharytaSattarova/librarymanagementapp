﻿using Library.DAL.Context;
using Library.DAL.Entities;
using Library.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    /// <summary>
    /// Serves as the base class for all the repositories.
    /// </summary>
    /// <typeparam name="TEntity">Generic parameter which specifies the entity type.</typeparam>
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// Stores the instance of the application context containing collections of all comments, news stories and survey results present in the system.
        /// </summary>
        protected readonly ApplicationContext context;

        /// <summary>
        /// Constructor that creates instance of the base repository based on the context passed as an argument.
        /// </summary>
        /// <param name="context">Application context instance with seed information.</param>
        protected BaseRepository(ApplicationContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Adds provided entity to the collection of entities of the same type.
        /// </summary>
        /// <param name="entity">Entity instance to add to the collection.</param>
        public void Create(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
            context.SaveChanges();
        }

        /// <summary>
        /// Deletes the given entity instance from the collection of entities of the same type.
        /// </summary>
        /// <param name="entity">Entity instance to delete from the collection.</param>
        public void Delete(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
            context.SaveChanges();
        }

        /// <summary>
        /// Searches for the entity by Id and deletes entity instance from the collection of entities of the same type if it's found.
        /// </summary>
        /// <param name="id">Id of the entity that has to be deleted.</param>
        public void Delete(int id)
        {
            var itemToDelete = context.Set<TEntity>().SingleOrDefault(x => x.ID == id);

            context.Set<TEntity>().Remove(itemToDelete);
            context.SaveChanges();
        }

        /// <summary>
        /// Gets all the entities of the specified type.
        /// </summary>
        /// <returns>List of entities.</returns>
        public List<TEntity> Get()
        {
            return context.Set<TEntity>().ToList();
        }

        /// <summary>
        /// Gets the entity with specified Id.
        /// </summary>
        /// <param name="id">Id of the entity to find and return.</param>
        /// <returns>Entity or null if the entity with given Id does not exist in the collection.</returns>
        public TEntity Get(int id)
        {
            return context.Set<TEntity>().Where(x => x.ID == id).FirstOrDefault();
        }

        /// <summary>
        /// Searches for an entity by Id and updates it's information with the information provided in the entity object argument.
        /// </summary>
        /// <param name="entity">Entity instance containig new entity information.</param>
        /// <param name="id">Id of the entity that has to be updated.</param>
        public virtual void Update(TEntity entity, int id)
        {
            context.SaveChanges();
        }
    }
}
