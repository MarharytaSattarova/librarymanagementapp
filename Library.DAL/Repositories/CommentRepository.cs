﻿using Library.DAL.Context;
using Library.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    /// <summary>
    /// Repository to access and modify comment instances in the application context.
    /// </summary>
    public class CommentRepository : BaseRepository<Comment>
    {
        /// <summary>
        /// Construstor that creates an instance of the comment repository based on the ApplicationContext instance passed as argument.
        /// </summary>
        /// <param name="context">Application context instance with seed information.</param>
        public CommentRepository(ApplicationContext context) : base(context) { }

        /// <summary>
        /// Searches for a comment by Id and updates it's information with the information provided in the comment object argument.
        /// </summary>
        /// <param name="entity">comment instance containig new comment information.</param>
        /// <param name="id">Id of the comment that has to be updated.</param>
        /// <exception cref="ArgumentException">Thrown when comment with the provided Id is not found."</exception>
        public override void Update(Comment entity, int id)
        {
            var searchedComment = context.Set<Comment>().FirstOrDefault(x => x.ID == id);

            if (searchedComment == null) throw new ArgumentException("Comment with such ID not found.", "id");

            searchedComment.AuthorName = entity.AuthorName;
            searchedComment.PublicationDate = entity.PublicationDate;
            searchedComment.Body = entity.Body;
        }
    }
}
