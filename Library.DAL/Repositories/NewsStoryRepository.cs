﻿using Library.DAL.Context;
using Library.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    /// <summary>
    /// Repository to access and modify news story instances in the application context.
    /// </summary>
    public class NewsStoryRepository : BaseRepository<NewsStory>
    {
        /// <summary>
        /// Construstor that creates an instance of the news story repository based on the ApplicationContext instance passed as argument.
        /// </summary>
        /// <param name="context">Application context instance with seed information.</param>
        public NewsStoryRepository(ApplicationContext context) : base(context) { }

        /// <summary>
        /// Searches for a news story by Id and updates it's information with the information provided in the news story object argument.
        /// </summary>
        /// <param name="entity">News story instance containig new news story information.</param>
        /// <param name="id">Id of the news story that has to be updated.</param>
        /// <exception cref="ArgumentException">Thrown when news story with the provided Id is not found."</exception>
        public override void Update(NewsStory entity, int id)
        {
            var searchedNewsStory = context.Set<NewsStory>().FirstOrDefault(x => x.ID == id);

            if (searchedNewsStory == null) throw new ArgumentException("News Story with such ID not found.", "id");

            searchedNewsStory.AuthorName = entity.AuthorName;
            searchedNewsStory.AuthorSurname = entity.AuthorSurname;
            searchedNewsStory.PublicationDate = entity.PublicationDate;
            searchedNewsStory.Title = entity.Title;
            searchedNewsStory.Body = entity.Body;
        }
    }
}
